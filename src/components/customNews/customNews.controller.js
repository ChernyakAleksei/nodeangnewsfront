import ng from 'angular';

export default class customNews {
  constructor(NewsGetter, $stateParams, $timeout) {
    'ngInject';
    this.NewsGetter = NewsGetter;
    this.id = $stateParams.id;
    this.$timeout = $timeout;
  }
  $onInit() {
    this.containerNews = ng.element(document.querySelector('#conteiner-for-news'));
    this.init();
  }
  init() {
    this.NewsGetter.getsingleNews(this.id)
    .then((resp) => { this.news = resp[0]; return resp[0]; })
    .then((data) => { this.containerNews.html(data.html); });
  }
}
