import ng from 'angular';
import customNewsComponent from './customNews.component';

export default ng.module('app.components.customNews', [])
    .component('customNews', customNewsComponent).name;
