import template from './customNews.html';
import controller from './customNews.controller';

export default {
  bindings: { mydata: '<' },
  template,
  controller
};
