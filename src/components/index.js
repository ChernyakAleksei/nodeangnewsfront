import ng from 'angular';
import listNews from './listNews';
import footerComponent from './footer';
import headerComponent from './header';
import singleNews from './singleNews';
import customNews from './customNews';
import NewsGetter from '../services/newsGetter.service';


export default ng.module('app.components', [listNews, footerComponent,
headerComponent, singleNews, customNews])
.service('NewsGetter', NewsGetter).name;
