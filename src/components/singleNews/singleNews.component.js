import template from './singleNews.html';

export default {
  bindings: {
    news: '<',
  },
  template
};
