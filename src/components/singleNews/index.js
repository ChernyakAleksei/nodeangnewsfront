import ng from 'angular';
import singleNewsComponent from './singleNews.component';

export default ng.module('app.components.singleNews', [])
    .component('singleNews', singleNewsComponent).name;
