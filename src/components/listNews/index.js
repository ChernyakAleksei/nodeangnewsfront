import ng from 'angular';
import listNewsComponent from './listNews.component';

export default ng.module('app.components.listNews', [])
    .component('listNews', listNewsComponent).name;
