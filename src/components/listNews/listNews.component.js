import template from './listNews.html';
import controller from './listNews.controller';

export default {
  bindings: {
    allNews: '<'
  },
  template,
  controller
};
