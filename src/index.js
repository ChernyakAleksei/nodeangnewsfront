import 'bootstrap/less/bootstrap.less';
import 'font-awesome/less/font-awesome.less';
import 'open-sans-fontface/open-sans.less';
import ng from 'angular';
import ngRoute from 'angular-ui-router';
import './style/main.scss';
import NgComponents from './components';
import routing from './routs';

ng.module('app', [ngRoute, NgComponents])
.config(routing);
