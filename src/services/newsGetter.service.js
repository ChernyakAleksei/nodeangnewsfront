export default class NewsGetter {
  constructor($http, $state) {
    'ngInject';
    this.$http = $http;
    this.$state = $state;
  }

  getsingleNews(id) {
    const path = `${API}/${id}`; // API global value from Webpack configuration
    return this.httpReq(path);
  }
  getNews() {
    const path = `${API}`;
    return this.httpReq(path);
  }
  httpReq(path) {
    return this.$http.get(path).then(resp => resp.data);
  }
}
