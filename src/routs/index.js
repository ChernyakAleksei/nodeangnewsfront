export default function routing($locationProvider, $stateProvider) {
  'ngInject';
  $locationProvider.html5Mode(true);
  const states = [
    {
      name: 'home',
      url: '/',
      component: 'listNews',
      resolve: {
        allNews: ['NewsGetter', NewsGetter => NewsGetter.getNews()]
      }
    },
    {
      name: 'customNews',
      url: '/list/:id',
      component: 'customNews',
    }
  ];
  states.forEach(state => $stateProvider.state(state));
}
